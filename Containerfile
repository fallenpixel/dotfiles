ARG BUILD_REPO
ARG BUILD_USER
FROM ${BUILD_REPO:-library}/alpine:3.17
ARG HELM_VERSION=3.2.1
ARG YQ_VERSION=v4.33.3
ARG ZARF_VERSION=v0.25.2
ARG LUALS_VERSION=3.6.19
RUN adduser -D -u 1000 -s /bin/zsh ${BUILD_USER:-lehmann.eric} &&\
    apk add --no-cache \
      ansible \
      bash \
      bash-completion \
      binutils \
      bind-tools \
      build-base \
      ca-certificates \
      coreutils \
      curl \
      findutils \
      fping \
      git \
      grep \
      gzip \
      helm \
      iproute2 \
      iputils \
      jq \
      k9s \
      less \
      mtr \
      mysql-client \
      net-tools \
      neovim \
      nmap \
      npm \
      netcat-openbsd \
      openssl \
      openssh-client \
      py3-pip \
      python3 \
      stow \
      tcpdump \
      unzip \
      util-linux \
      wget \
      zsh
RUN curl -LO \
  "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
  install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && \
  rm kubectl && \
  curl -o /usr/local/bin/mc -L \
    https://dl.min.io/client/mc/release/linux-amd64/mc && \
  chmod 0755 /usr/local/bin/mc && \
  curl -o /usr/local/bin/zarf -L \
    https://github.com/defenseunicorns/zarf/releases/download/${ZARF_VERSION}/zarf_${ZARF_VERSION}_Linux_amd64 && \
  chmod 0755 /usr/local/bin/zarf && \
  curl -o /usr/local/bin/yq -L \
    https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 && \
  chmod 755 /usr/local/bin/yq
USER 1000
WORKDIR /home/${BUILD_USER:-lehmann.eric}
COPY --chown=lehmann.eric . /home/${BUILD_USER:-lehmann.eric}/.dotfiles
COPY --chown=root:root --chmod=0755 ./entrypoint.sh /entrypoint.sh
RUN git -C .dotfiles submodule update --init && \
  find .dotfiles -type f -exec chmod 644 {} \; && \
  find .dotfiles -type d -exec chmod 755 {} \; && \
  chmod +x .dotfiles/entrypoint.sh && \
  stow -d .dotfiles zsh && \
  stow -d .dotfiles nvim && \
  touch $HOME/.containerized && \
  /bin/zsh -c 'source ~/.zshrc' && \
  ansible-galaxy collection install community.general ansible.posix && \
  mkdir -p $HOME/.completions/ && \
  yq shell-completion zsh > $HOME/.completions/_yq &&\
  kubectl completion zsh > $HOME/.completions/_kubectl
ENTRYPOINT zsh -c -- /entrypoint.sh
