local ls = require("luasnip")
local extras = require("luasnip.extras")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local l = require("luasnip.extras").lambda
local rep = require("luasnip.extras").rep
local p = require("luasnip.extras").partiaL
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local types = require("luasnip.util.types")
local conds = require("luasnip.extras.conditions")
local conds_expand = require("luasnip.extras.conditions.expand")
local function get_path_name()
end
return {
  s("k-fluxrelease",
  fmt(
  [[
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: {}
  namespace: flux-system
spec:
  interval: 5m
  timeout: 10m
  chart:
    spec:
      chart: chart
      sourceRef:
        kind: {}
        name: {}
        namespace: flux-system
      interval: 1m
  valuesFrom:
  - kind: ConfigMap
    name: {}-values
    valuesKey: base
    optional: true
  - kind: ConfigMap
    name: {}-values
    valuesKey: project-overlay
    optional: true
  - kind: ConfigMap
    name: {}-values
    valuesKey: instance-overlay
    optional: true
  targetNamespace: {}
  install:
    createNamespace: false
]],
  {
    i(1, 'appname'),
    c(2, {
      t("GitRepository"),
      t("HelmRepository")
    }),
    i(3, 'reponame'),
    extras.rep(1),
    extras.rep(1),
    extras.rep(1),
    i(4, 'targetnamespace')
  }
  )),
  s("k-fluxrepo",
  fmt(
  [[
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: {}
metadata:
  name: {}
  namespace: flux-system
spec:
  interval: 30m0s
  url: {}
  ]],
  {
    c(1, {
      t("HelmRepository"),
      t("GitRepository")
    }),
    i(2, "reponame"),
    i(3, "repourl")
  }
  )),
  s("k-kustomize",
  fmt(
  [[
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: {}
components:
  - {}
  ]],
  {
    i(1, "default"),
    i(2, "lighthouse")
  }
  )),
  s("zarf",
  fmt(
  [[
kind: ZarfPackageConfig
metadata:
  name: {}
  description: {}
components:
- name: {}
  description: {}
  required: {}
  manifests:
  - name {}-manifests
  kustomizations:
  - {}
  images:
  - {}
  ]],
  {
    c(1, {
      f(function () return vim.fn.expand('%:p:h:t') end),
      i(1, 'package')
    }),
    i(2, "Description"),
    extras.rep(1),
    i(3, "Description"),
    c(4, {
      t("true"),
      t("false")
    }),
    extras.rep(1),
    c(5, {
      t("lighthouse"),
      t("lighthouse-offline"),
      i(1, "kustomizedir")
    }),
    i(6, "image")
  }
  ))
}

