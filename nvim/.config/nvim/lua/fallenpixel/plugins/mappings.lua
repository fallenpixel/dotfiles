return {
	"folke/which-key.nvim",
	config = function()
		local map = vim.keymap.set
		local wk = require("which-key")
    local mappings = {
      { "<leader>A", "<cmd>lua require('notify').dismiss()<cr>", desc = "Dismiss notifications", nowait = true, remap = false },
      { "<leader>E", "viWc<c-r>=system('base64 --decode', @\")<cr><esc>$", desc = "Base64 decode", nowait = true, remap = false },
      { "<leader>H", "<cmd>vertical resize -10<cr>", desc = "Vert Smaller", nowait = true, remap = false },
      { "<leader>J", "<cmd>vertical resize -10<cr>", desc = "Horiz Smaller", nowait = true, remap = false },
      { "<leader>K", "<cmd>vertical resize +10<cr>", desc = "Horiz Bigger", nowait = true, remap = false },
      { "<leader>L", "<cmd>vertical resize +10<cr>", desc = "Vert Bigger", nowait = true, remap = false },
      { "<leader>M", "<cmd>Glow<cr>", desc = "Preview Markdown", nowait = true, remap = false },
      { "<leader>P", "<cmd>r !pwgen -sy -n 20 1<cr>", desc = "PWGen", nowait = true, remap = false },
      { "<leader>a", "<cmd>Telescope notify<cr>", desc = "Notifications", nowait = true, remap = false },
      { "<leader>b", "<cmd>Telescope buffers theme=cursor<cr>", desc = "Buffers", nowait = true, remap = false },
      { "<leader>d", "<cmd>lua vim.diagnostic.open_float()<cr>", desc = "Diagnostics", nowait = true, remap = false },
      { "<leader>e", "viWc<c-r>=system('base64 -w 0 ', @\")<cr><esc>$", desc = "Base64 encode", nowait = true, remap = false },
      { "<leader>g", group = "Git", nowait = true, remap = false },
      { "<leader>gR", "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", desc = "Reset Buffer", nowait = true, remap = false },
      { "<leader>gW", "<cmd> Telescope git_worktree create_git_worktree<cr>", desc = "New worktree", nowait = true, remap = false },
      { "<leader>gb", "<cmd>Telescope git_branches<cr>", desc = "Checkout branch", nowait = true, remap = false },
      { "<leader>gc", "<cmd>Telescope git_commits<cr>", desc = "Checkout commit", nowait = true, remap = false },
      { "<leader>gd", "<cmd>lua require 'gitsigns'.diffthis()<CR>", desc = "DiffView", nowait = true, remap = false },
      { "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", desc = "Lazygit", nowait = true, remap = false },
      { "<leader>gj", "<cmd>lua require 'gitsigns'.next_hunk()<cr>", desc = "Next Hunk", nowait = true, remap = false },
      { "<leader>gk", "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", desc = "Prev Hunk", nowait = true, remap = false },
      { "<leader>gl", "<cmd>lua require 'gitsigns'.blame_line()<cr>", desc = "Blame", nowait = true, remap = false },
      { "<leader>go", "<cmd>Telescope git_status<cr>", desc = "Open changed file", nowait = true, remap = false },
      { "<leader>gp", "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", desc = "Preview Hunk", nowait = true, remap = false },
      { "<leader>gr", "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", desc = "Reset Hunk", nowait = true, remap = false },
      { "<leader>gs", "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", desc = "Stage Hunk", nowait = true, remap = false },
      { "<leader>gu", "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>", desc = "Undo Stage Hunk", nowait = true, remap = false },
      { "<leader>gw", "<cmd> Telescope git_worktree git_worktrees<cr>", desc = "Existing worktrees", nowait = true, remap = false },
      { "<leader>k", "<cmd>lua vim.lsp.buf.hover()<cr>", desc = "Hover", nowait = true, remap = false },
      { "<leader>l", group = "LSP", nowait = true, remap = false },
      { "<leader>lP", "<cmd> lua vim.lsp.buf.list_workspace_folders()<cr>", desc = "List workspace folders", nowait = true, remap = false },
      { "<leader>lS", "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", desc = "Workspace Symbols", nowait = true, remap = false },
      { "<leader>lT", "<cmd>InspectTree<cr>", desc = "Inspect Treesitter", nowait = true, remap = false },
      { "<leader>la", "<cmd>lua vim.lsp.buf.code_action()<cr>", desc = "Code Action", nowait = true, remap = false },
      { "<leader>ld", "<cmd>Telescope diagnostics<cr>", desc = "Document Diagnostics", nowait = true, remap = false },
      { "<leader>lf", "<cmd>lua vim.lsp.buf.format()<cr>", desc = "Format", nowait = true, remap = false },
      { "<leader>li", "<cmd>LspInfo<cr>", desc = "Info", nowait = true, remap = false },
      { "<leader>lj", "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>", desc = "Next Diagnostic", nowait = true, remap = false },
      { "<leader>lk", "<cmd>lua vim.lsp.diagnostic.goto_prev()<cr>", desc = "Prev Diagnostic", nowait = true, remap = false },
      { "<leader>ll", "<cmd>lua vim.lsp.codelens.run()<cr>", desc = "CodeLens Action", nowait = true, remap = false },
      { "<leader>lp", "<cmd> lua vim.lsp.buf.add_workspace_folder()<cr>", desc = "Add workspace folder", nowait = true, remap = false },
      { "<leader>lq", "<cmd>lua vim.lsp.diagnostic.set_loclist()<cr>", desc = "Quickfix", nowait = true, remap = false },
      { "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<cr>", desc = "Rename", nowait = true, remap = false },
      { "<leader>ls", "<cmd>Telescope lsp_document_symbols<cr>", desc = "Document Symbols", nowait = true, remap = false },
      { "<leader>lt", "<cmd>Telescope treesitter<cr>", desc = "Treesitter symbols", nowait = true, remap = false },
      { "<leader>lx", "<cmd>lua vim.lsp.buf.remove_workspace_folder()<cr>", desc = "Remove workspace folder", nowait = true, remap = false },
      { "<leader>m", group = "Modelines", nowait = true, remap = false },
      { "<leader>ma", "<cmd>set ft=yaml.ansible ts=2 et<cr>ggO# vim:ft=yaml.ansible ts=2 et<esc><C-o>", desc = "Ansible", nowait = true, remap = false },
      { "<leader>mk", "<cmd>set ft=yaml ts=2 et<cr>ggO# vim:ft=yaml ts=2 et<enter> yaml-language-server: $schema=https://github.com/yannh/kubernetes-json-schema/raw/master/v1.24.14-standalone/all.json", desc = "Kubernetes", nowait = true, remap = false },
      { "<leader>n", group = "Neorg", nowait = true, remap = false },
      { "<leader>nf", "<cmd>Telescope neorg find_linkable<cr>", desc = "Find linkable", nowait = true, remap = false },
      { "<leader>ni", "<cmd>Neorg index<cr>", desc = "Open Index", nowait = true, remap = false },
      { "<leader>nj", "<cmd>Neorg journal today<cr>", desc = "Open Index", nowait = true, remap = false },
      { "<leader>nm", "<cmd>Neorg inject-metadata<cr>", desc = "Inject Metadata", nowait = true, remap = false },
      { "<leader>nr", "<cmd>Neorg return<cr>", desc = "Close all neorg files", nowait = true, remap = false },
      { "<leader>p", group = "Packer", nowait = true, remap = false },
      { "<leader>ph", "<cmd>Lazy home<cr>", desc = "Home", nowait = true, remap = false },
      { "<leader>pi", "<cmd>Lazy install<cr>", desc = "Install", nowait = true, remap = false },
      { "<leader>pm", "<cmd>Mason<cr>", desc = "Update LSP", nowait = true, remap = false },
      { "<leader>ps", "<cmd>Lazy sync<cr>", desc = "Sync", nowait = true, remap = false },
      { "<leader>pu", "<cmd>Lazy update<cr>", desc = "Update", nowait = true, remap = false },
      { "<leader>q", "<cmd>nohlsearch<CR>", desc = "No Highlight", nowait = true, remap = false },
      { "<leader>t", "<cmd>NvimTreeToggle<cr>", desc = "Nvim Tree", nowait = true, remap = false },
      { "<leader>v", group = "Vim Configs", nowait = true, remap = false },
      { "<leader>ve", "<cmd>lua require('telescope.builtin').find_files({cwd=os.getenv('DOTFILES')})<cr>", desc = "Fuzzyfind Dotfiles", nowait = true, remap = false },
      { "<leader>vf", "<cmd>source %<cr>", desc = "Reload current file", nowait = true, remap = false },
      { "<leader>vr", "<cmd>Telescope reloader<cr>", desc = "Reload modules", nowait = true, remap = false },
      { "<leader>yk", "<cmd>YAMLYankKey<cr>", desc = "Yank Key", nowait = true, remap = false },
      { "<leader>ys", "<cmd>Telescope yaml_schema<cr>", desc = "Yaml Schema", nowait = true, remap = false },
      { "<leader>yt", "<cmd>YAMLTelescope<cr>", desc = "YAML Telescope", nowait = true, remap = false },
      { "<leader>yv", "<cmd>YAMLYankValue<cr>", desc = "Yank Value", nowait = true, remap = false },
      { "<leader>z", group = "Spelling", nowait = true, remap = false },
      { "<leader>zs", "<cmd>set spell!<cr>", desc = "Toggle spelling", nowait = true, remap = false },
      { "<leader>zz", "<cmd>Telescope spell_suggest theme=cursor<cr>", desc = "Suggestions", nowait = true, remap = false },
      { "<leader>e", "c<c-r>=system('base64 -w 0 ', @\")<cr><esc>$", mode = "v", desc = "Base64 Encode"},
      { "<leader>E", "c<c-r>=system('base64 -w 0 ', @\")<cr><esc>$", mode = "v", desc = "Base64 Encode"},
		}
    wk.add(mappings)
		map("n", "<C-t>h", ":tabr<cr>")
		map("n", "<C-t>l", ":tabl<cr>")
		map("n", "<C-t>j", ":tabn<cr>")
		map("n", "<C-t>k", ":tabp<cr>")
		map("n", "<C-t>t", ":tabnew<cr>")
		map("i", "jk", "<ESC>")
		map("n", "<C-h>", "<C-w>h")
		map("n", "<C-j>", "<C-w>j")
		map("n", "<C-k>", "<C-w>k")
		map("n", "<C-l>", "<C-w>l")
		map("n", "ga", "<Plug>(EasyAlign)")
		map("x", "ga", "<Plug>(EasyAlign)")
		map("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>")
		map("n", "[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>")
		map("n", "]d", "<cmd>lua vim.diagnostic.goto_next()<CR>")
		map("n", "<C-a>", "<Plug>(dial-increment)")
		map("v", "<C-a>", "<Plug>(dial-increment)")
		map("n", "<C-x>", "<Plug>(dial-decrement)")
		map("v", "<C-x>", "<Plug>(dial-decrement)")
		map("v", "g<C-a>", "<Plug>(dial-increment-additional)")
		map("v", "g<C-x>", "<Plug>(dial-decrement-additional)")
    map("n", "<leader>c", "<Plug>OSCYankOperator")
    map("v", "<leader>c", "<Plug>OSCYankVisual")
	end,
}
