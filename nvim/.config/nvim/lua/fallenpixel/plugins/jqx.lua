return {
  {
    'gennaro-tedesco/nvim-jqx', ft = {"json", "yaml"},
  },
  {
    "cuducos/yaml.nvim",
    ft = { "yaml" },
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim"
    }
  }
}
