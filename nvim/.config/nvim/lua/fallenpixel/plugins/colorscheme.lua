return {
    "marko-cerovac/material.nvim",
    lazy = false,
    priority = 1000,
    opts = {
      styles = {
        comments = { italic = true },
        strings = { bold = true },
      },
      plugins = {
        "nvim-tree",
        "gitsigns",
        "neorg",
        "nvim-cmp",
        "nvim-web-devicons",
        "telescope",
        "which-key"
      },
      lualine_style = "stealth"
    },
    config = function (_, opts)
      require("material").setup(opts)
      vim.g.material_style = "darker"
      vim.cmd([[colorscheme material]])
    end
  }
