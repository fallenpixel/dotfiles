return {
  {'lewis6991/gitsigns.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim'
    },
    config = true
  },
  {'sindrets/diffview.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim'
    }
  },
  {'ThePrimeagen/git-worktree.nvim',
    config = true
  },
  {'ruifm/gitlinker.nvim',
    config = true
  }
}

