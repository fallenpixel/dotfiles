return { 'epwalsh/obsidian.nvim',
  lazy = true,
  event = {
   "BufReadPre " .. vim.fn.expand "~" .. "/notes/**.md",
   "BufNewFile " .. vim.fn.expand "~" .. "/notes/**.md",
   },
   dependencies = { "nvim-lua/plenary.nvim"},
   opts = {
     workspaces = {
       {
         name = "work",
         path = "~/notes"
       }
     }
   }
}
