local nogcc = {'winterfell'}
local function hasgcc(tab, val)
  for _ , value in ipairs(tab) do
    if value == val then
      return false
    end
  end
  return true
end
local install_parsers
if hasgcc(nogcc, vim.fn.hostname()) then
  install_parsers = {
    "bash",
    "c",
    "cpp",
    "gitignore",
    "javascript",
    "json",
    "lua",
    "markdown",
    "markdown_inline",
    "python",
    "regex",
    "toml",
    "terraform",
    "tsx",
    "typescript",
    "vim",
    "yaml"
  }
else
  install_parsers = {}
end
return {
  {'nvim-treesitter/nvim-treesitter',
    build = ":TSUpdate",
    dependencies = {
      'https://gitlab.com/HiPhish/rainbow-delimiters.nvim.git',
      'nvim-treesitter/nvim-treesitter-textobjects',
      'nvim-treesitter/nvim-treesitter-context'
    },
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
      vim.opt.foldlevel = 100
      vim.opt.foldmethod = "expr"
      vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
    end,
    opts = {
        ensure_installed = install_parsers,
        auto_install = true,
        sync_install = true,
        highlight = {
          enable = true,
          additional_vim_regex_highlighting = true,
        },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = "<Leader>ss",
            node_incremental = "<Leader>si",
            scope_incremental = "<Leader>sc",
            node_decremental = "<Leader>sd",
          },
        },
        indent = {
          enable = false,
          -- disable = { "yaml"},
        },
        context_commentstring = {
          enable = true,
          enable_autocmd = false,
        },
        refactor = {
          highlight_definitions = {
          enable = true,
          clear_on_cursor_move = true
          },
          highlight_current_scope = { enable = true },
          smart_rename = {
              enable = true,
              keymaps = {
                smart_rename = "grr",
              }
          },
          navigation = {
            enable = true,
            keymaps = {
              goto_definition_lsp_fallback = "gnd",
              list_definitions = "gnD",
              list_definitions_toc = "gO",
              goto_next_usage = "<A-8>",
              goto_previous_usage = "<A-3>",
            },
          },
        },
        textobjects = {
          select = {
            enable = true,
            lookahead = true,
            keymaps = {
              ["av"] = "@assignment.outer",
              ["iv"] = "@assignment.rhs",
              ["ak"] = "@assignment.outer",
              ["ik"] = "@assignment.lhs",
              ["af"] = "@function.outer",
              ["if"] = "@function.inner",
              ["ac"] = "@class.outer",
              ["ic"] = "@class.inner",
            },
            selection_modes = {
              ['@parameter.outer'] = 'v', -- charwise
              ['@function.outer'] = 'V', -- linewise
              ['@class.outer'] = '<c-v>', -- blockwise
            },
            include_surrounding_whitespace = true,
          },
        },
        rainbow = {
          enable = true,
          extended_mode = true
        },
      }
  },
  {'nvim-treesitter-context',
    opts = {
      max_lines = "5"
    }
  }
}
