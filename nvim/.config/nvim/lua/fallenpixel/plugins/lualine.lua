return {
	"nvim-lualine/lualine.nvim",
	dependencies = {
		"kyazdani42/nvim-web-devicons",
		"marko-cerovac/material.nvim",
		"someone-stole-my-name/yaml-companion.nvim",
	},
	config = true,
	opts = {
		options = {
			section_separators = { left = "", right = "" },
			component_separators = { left = "", right = "" },
			theme = "material",
		},
		sections = {
			lualine_x = {
				function()
					local schema = require("yaml-companion").get_buf_schema(0)
					if schema.result[1].name == "none" then
						return ""
					end
					return schema.result[1].name
				end,
				"encoding",
				"fileformat",
				"filetype",
			},
		},
	},
}
