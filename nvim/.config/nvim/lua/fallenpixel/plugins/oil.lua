return {
	{
		"stevearc/oil.nvim",
		config = true,
    dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {
			columns = {
				"icon",
				"permissions",
				"size",
				"mhome",
			},
		},
	},
}
