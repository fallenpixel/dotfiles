return {
  {
    'akinsho/toggleterm.nvim',
    config = true,
    opts = {
      open_mapping = [[<c-\>]]
    }
  },
}
