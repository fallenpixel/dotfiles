return {
  { 'windwp/nvim-autopairs',
    dependencies = {
      'hrsh7th/nvim-cmp'
    },
    opts = {
          check_ts = true,
          ts_config = {
            lua = { "string", "source" },
          },
          disable_filetype = { "TelescopePrompt" },
          fast_wrap = {
            map = "<c-s>",
            chars = { "{", "[", "(", '"', "'" },
            pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], "%s+", ""),
            offset = 0,
            end_key = "$",
            keys = "qwertyuiopzxcvbnmasdfghjkl",
            check_comma = true,
            highlight = "PmenuSel",
            highlight_grey = "LineNr",
          }
        },
    config = function(_, opts)
      require("nvim-autopairs").setup(opts)
      local cmp_autopairs = require("nvim-autopairs.completion.cmp")
      local cmp = require("cmp")
      cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done {map_char = {tex = ""} })
    end
  }
}
