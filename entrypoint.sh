#!/bin/bash

if [[ -n $KUBERNETES_SERVICE_HOST ]]; then
  echo "Detected a K8s environment, sleeping"
  while true; do
    echo "Sleeping..."
    sleep 60
  done
else zsh
fi
