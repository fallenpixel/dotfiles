#!/usr/bin/env zsh
# Load Nix profile if it exists
#
if [[ -n $NIX_PATH && -f "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" ]]; then
  source $HOME/.nix-profile/etc/profile.d/hm-session-vars.sh
fi

# XDG base directory specification
export XDG_CONFIG_HOME=$HOME/.config

# Aliases for editors
alias vi="nvim"
alias vim="nvim"
alias svi="sudoedit"
alias se="sudoedit"

# Command aliases based on available commands
for cmd in bat kubecolor exa; do
  if which command $cmd > /dev/null 2>&1; then
    case $cmd in
      bat) alias cat='bat' ;;
      kubecolor) alias kubectl='kubecolor' ;;
      exa) alias ls='exa' ;;
    esac
  fi
done

# Conditional configurations for specific hosts
if [[ "$HOST" = shadow ]]; then
  export LIBVIRT_DEFAULT_URI="qemu+ssh://katyl@casterlyrock/system"
  export DOCKER_HOST=ssh://katyl@casterlyrock
fi

# Extend PATH with custom directories
if [[ -d ~/.bin/ ]]; then
  export PATH=$PATH:~/.bin
fi
if [[ -d ~/.local/bin ]]; then
  export PATH=$PATH:~/.local/bin
fi
if [[ -d ~/.go/bin ]]; then
  export PATH=$PATH:~/.go/bin
fi

# Configuration files
if [[ -f "$HOME/.ripgreprc" ]]; then
  export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"
fi
if [[ -d "$HOME/.completions" ]]; then
  export FPATH=$HOME/.completions:$FPATH
fi
if [[ -f "$HOME/.zarf-config.toml" ]]; then
  export ZARF_CONFIG="$HOME/.zarf-config.toml"
fi

# Kubernetes aliases
alias k="kubectl"
alias template="kubectl create --dry-run=client -o yaml"

# Environment variables and aliases for system utilities
export SUDO_PROMPT="Enter password:  "
export SUDO_EDITOR="nvim"
export ANSIBLE_NOCOWS=1
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=6"
export EDITOR="nvim"
export SYSTEMD_EDITOR="nvim"
export PAGER="less"
export LESS="-iFXR"
export DIFFPROG="nvim -d"
export MANPAGER="nvim +Man!"
export GOPATH="${HOME}/.go"
export FZF_DEFAULT_COMMAND='rg --no-messages --files --no-ignore --hidden --follow --glob "!.git/*"'
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border'
alias top="htop"
alias cd=" cd"
alias sudo="sudo "
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias codemusic="mpv --no-video --shuffle 'https://www.youtube.com/playlist?list=PLUja9J5M1XReqoBal5IKog_PWz2Q_hZ7Y'"
alias shantytime="mpv --no-video -shuffle 'https://www.youtube.com/playlist?list=PLfxnB1YXnxp7ADOru6TZAv1sfQhE-7ht7'"
export TMPDIR='/tmp/'

# History configuration
setopt EXTENDED_HISTORY
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_VERIFY
export HISTFILE=~/.histfile
export HISTSIZE=10000
export SAVEHIST=10000
export DIRCOLORS=truecolor
setopt incappendhistory
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)

# Completion configuration
zstyle ':completion:*' completer _complete _ignored _correct #_approximate
autoload edit-command-line
zle -N edit-command-line
if [[ -f ~/.zshrc.local ]]; then
  source ~/.zshrc.local
fi
autoload -Uz compinit && compinit
if [[ -z $DOTFILES ]]; then
 DOTFILES="${HOME}/.dotfiles"
fi
# Kubectl completions
if which kubectl > /dev/null 2>&1; then
  if [[ ! -d $HOME/.completions/ ]]; then
    mkdir "$HOME/.completions/"
  fi
  if [[ ! -f $HOME/.completions/_kubectl ]]; then
    kubectl completion zsh > "$HOME/.completions/_kubectl"
  fi
  compdef kubecolor=kubectl
fi

# Prompt initialization
unsetopt BEEP

# FZF and key-bindings configuration
function zvm_after_init() {
  if [[ -f /usr/share/fzf/shell/key-bindings.zsh ]]; then
    source /usr/share/fzf/shell/key-bindings.zsh
    zvm_bindkey viins '^R' 'fzf-history-widget'
    zvm_bindkey vicmd '^R' 'fzf-history-widget'
    bindkey -M viins '\ev' 'fzf-cd-widget'
    bindkey -M vicmd '\ev' 'fzf-cd-widget'
    if [[ -f /usr/share/fzf/shell/completion.zsh ]]; then
      source /usr/share/fzf/shell/completion.zsh
    fi
  elif [[ -n "${commands[fzf-share]}" ]]; then
    source "$(fzf-share)/key-bindings.zsh"
    source "$(fzf-share)/completion.zsh"
    bindkey -M viins '\ev' 'fzf-cd-widget'
    bindkey -M vicmd '\ev' 'fzf-cd-widget'
  fi
  bindkey -M viins '\ej' 'jq-complete'
  bindkey -M vicmd '\ej' 'jq-complete'
  bindkey -M vicmd v edit-command-line
}
function set_pass(){
  if [[ $# != 1 ]]; then
    echo "Pass the envvar you want to set as the first and only arg"
    return 1
  fi
  _var=$1
  read -s "?${_var}=" ${_var=}
  export ${_var}
}
source $DOTFILES/antidote/antidote.zsh
antidote load $HOME/.zsh_plugins.txt
eval "$(starship init zsh)"
# vim: ft=sh
